<?php include("db/Module.php");?>

<!--
Auteur: Lucas Perrault
Date: 19/05/2018
Intitulé: Exercice Horloge numérique en PHP / AJAX
-->

<!DOCTYPE html>
<html>
<head>
	<title>Horloge</title>
	<!--Définition de la vue/zoom pour l'affichage mobile-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!--Appel des librairies-->
	<?php module::bootstrap(); ?>
	<?php module::librairie(); ?>
	<?php module::jquery_ajax(); ?>
</head>
<body>
	<!--Container principal avec un GIF full screen-->
	<div class="container-fluid fond">
		<!--Affichage du temps/logo-->
		<div class="time">
			<div class="display-4" id="horloge"></div>
			<hr class="my-2">
			<img src="image/logo_hubone.png" width="60%">
			<hr class="my-2">
		</div>
		<!--Affichage photo de profil-->
		<div class="profil">
			<img class="profil_photo" src="image/photo_profil.JPG" width="20%" title="Lucas Perrault">
		</div>
	</div>

	<script>
		$(document).ready(function(){
			/*Je lance deux fois la fonction ajax. Sinon il y aurait un temps d'attente de 1 seconde pour son affichage*/
			// Chargement à l'ouverture
			$.ajax({
					// Dossier contenant la date
					url: 'db/comptage.php',
	      			success: function(data) {
	      				// Division où l'on affiche la date
	        			$("#horloge").html(data);
	        		}
				});
			// Chargement à chaque seconde
			setInterval(function(){ 
				$.ajax({
					url: 'db/comptage.php',
	      			success: function(data) {
	        			$("#horloge").html(data);
	        		}
				});
			},1000);
		});
	</script>
</body>
</html>